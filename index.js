const TurndownService = require('turndown')
const RssFeed = require('rss-feed-emitter')
const Eris = require('eris')
const decode = require('decode-html')
const Logger = require('./utils/Logger')
const Feeds = require('./utils/webhooks')
const config = require('./config')
const feeds = require('./feeds')

process.setMaxListeners(0)

const logger = new Logger(true, true, config.sentry.dsn)

const td = new TurndownService({
  headingStyle: 'atx',
  bulletListMarker: '-',
  codeBlockStyle: 'fenced',
  emDelimiter: '*'
})

// noinspection JSUnusedGlobalSymbols
td.addRule('cite', {
  filter: ['cite'],
  replacement: (content) => {
    return `*${content}*`
  }
})

td.addRule('img', {
  filter: ['img'],
  replacement: (content, node, options) => {
    let t = '[image]'

    if (node.getAttribute('title')) {
      t = node.getAttribute('title')
    } else if (node.getAttribute('alt')) {
      t = node.getAttribute('alt')
    }

    return `[${t}](${node.getAttribute('src')})`
  }
})

const client = new Eris()

const webhooks = new Feeds({
  logger: logger,
  turndown: td,
  client: client,
  feeds: feeds,
  decode: decode
})

const rssfeeds = new RssFeed()

feeds.forEach((feed) => {
  if (feed.name === 'Actus' && feed.urls.length > 0) {
    feed.urls.forEach((url) => rssfeeds.add({
      url: url,
      refresh: feed.refresh
    }))
  } else {
    if (feed.name !== 'Actus') {
      rssfeeds.add({
        url: feed.url,
        refresh: feed.refresh
      })
    }
  }
})

rssfeeds.on('new-item', async (item) => {
  if (process.uptime() < config.startDelay) return
  await webhooks.actus(item)
})

rssfeeds.on('error', (e) => {
  if (e.feed) {
    let feedName = e.feed
    logger.error('error', `${feedName}: ${e.message}`)
  } else {
    logger.error('error', e)
  }
})

process.on('uncaughtException', (e) => logger.error('error', e))
process.on('unhandledRejection', (e) => logger.error('error', e))

process.on('SIGINT', () => {
  logger.warn('close', 'Stopping listeners')
  rssfeeds.destroy()
  process.exit(0)
})

logger.init('open', 'Starting listeners')
logger.init('info', `Subscribed feeds:\n${rssfeeds.list().map((feed) => feed.url).join('\n')}`)
