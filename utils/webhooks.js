/**
 * TODO:
 * Look into [Seasonal releases]{@link http://www.senpai.moe/?season=spring2018&zone=Europe/Amsterdam&mode=table&air=stream}
 * And [Utils]{@link http://utils.senpai.moe/}
 */

class Feeds {
  /**
   * Feeds
   * @param {Object} options
   * @param {Logger} options.logger
   * @param {TurndownService} options.turndown
   * @param {Object} options.client
   * @param {Array} options.feeds
   */
  constructor (options) {
    this.logger = options.logger
    this.td = options.turndown
    this.client = options.client
    this.feeds = options.feeds
    this.decode = options.decode
  }

  /**
   * Execute webhook for "actus"
   * @param {Object} item
   * @return {Promise<void>}
   */
  async actus (item) {
    try {
      const feed = this.feeds.find((feed) => feed.name === 'Actus')
      this.logger.info('item', `Actus: ${item.title}`)

      if (item.description.substr(0, 4) === '&lt;') {
        item.description = this.decode(item.description)
      }

      // ffs
      if (item.description.includes('<!DOCTYPE html>')) {
        // eslint-disable-next-line
        const bodyRegex = /<body>(.*?)<\/body>/s
        let bM

        if ((bM = bodyRegex.exec(item.description)) !== null) {
          item.description = bM[1]
        }
      }

      let newDesc = this.td.turndown(item.description)
      if (newDesc.length > 2040) {
        newDesc = newDesc.substr(0, 2040) + '&hellip;'
      }

      let image = {}
      const imgRegex = /<img.*src="(.*?)".*\/?>/
      let iM
      if ((iM = imgRegex.exec(item.description)) !== null) {
        image = {
          url: iM[1]
        }
      }

      await this.client.executeWebhook(feed.id, feed.token, {
        username: item.meta.title,
        avatarURL: feed.avatar,
        embeds: [{
          title: item.title,
          url: item.link,
          description: newDesc,
          color: 0xe9184e,
          timestamp: item.date,
          image: image
        }]
      })
    } catch (e) {
      this.logger.error('webhook', e)
    }
  }
}

module.exports = Feeds
